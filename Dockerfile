FROM cloudron/base:0.9.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code
RUN curl -L http://wordpress.org/wordpress-4.6.1.tar.gz | tar -xz --strip-components 1 -f -
RUN mv /app/code/wp-content /app/code/wp-content-vanilla && ln -s /app/data/wp-content /app/code/wp-content
RUN ln -s /app/data/htaccess /app/code/.htaccess
ADD wp-config.php.template /app/code/wp-config.php.template

RUN mkdir -p /run/wordpress/sessions && \
    ln -sf /run/wordpress/wp-config.php /app/code/wp-config.php

RUN curl -L -o /app/code/wp https://github.com/wp-cli/wp-cli/releases/download/v0.24.1/wp-cli-0.24.1.phar
RUN chmod +x /app/code/wp
RUN /bin/echo -e "apache_modules:\n  - mod_rewrite" > /app/code/wp-cli.yml


# Get the plugins
RUN curl -L -o /app/code/authLdap.zip https://github.com/heiglandreas/authLdap/archive/1.4.19.zip
RUN curl -L -o /app/code/disable-wordpress-updates.zip https://downloads.wordpress.org/plugin/disable-wordpress-updates.1.4.7.zip
RUN curl -L -o /app/code/wp-mail-smtp.zip https://downloads.wordpress.org/plugin/wp-mail-smtp.0.9.5.zip

RUN chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache2-wordpress.conf /etc/apache2/sites-available/wordpress.conf
RUN ln -sf /etc/apache2/sites-available/wordpress.conf /etc/apache2/sites-enabled/wordpress.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite
RUN sed -e 's/upload_max_filesize = .*/upload_max_filesize = 500M/' \
        -e 's/post_max_size = .*/post_max_size = 500M/' \
        -e 's,;session.save_path.*,session.save_path = "/run/wordpress/sessions",' \
        -i /etc/php/7.0/apache2/php.ini

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]

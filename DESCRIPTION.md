This app packages WordPress 4.6.1.

WordPress is web software you can use to create a beautiful website or blog.
We like to say that WordPress is both free and priceless at the same time.

The core software is built by hundreds of community volunteers, and when
you’re ready for more there are thousands of plugins and themes available
to transform your site into almost anything you can imagine. Over 60 million
people have chosen WordPress to power the place on the web they call “home” 
— we’d love you to join the family.

### Apps

* [Android](https://play.google.com/store/apps/details?id=org.wordpress.android&hl=en)
* [iOS](https://itunes.apple.com/us/app/wordpress/id335703880?mt=8&uo=6&at=&ct=)


#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var firefox = require('selenium-webdriver/chrome');
    var server, browser = new firefox.Driver();
    var LOCATION = 'wptest';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    var email, token;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function login(done) {
        browser.get('https://' + app.fqdn + '/wp-login.php').then(function () {
            browser.sleep(2000); // there seems to be some javascript that gives auto-focus to username
            browser.findElement(by.id('user_login')).sendKeys(username);
            browser.findElement(by.id('user_pass')).sendKeys(password);
            browser.findElement(by.tagName('form')).submit();
            browser.wait(until.elementLocated(by.xpath('//h1[text()="Dashboard"]')), TIMEOUT).then(function () { done(); });
        });
    }

    function checkHtaccess(done) {
        var out = execSync('cloudron exec -- cat /app/data/htaccess');
        expect(out.toString('utf8').indexOf('RewriteEngine On')).to.not.be(-1); // wp generates this with permalinks in hard mode
        done();
    }

    function checkPermalink(done) {
        browser.get('https://' + app.fqdn + '/hello-world');
        browser.findElement(by.xpath('//h1[text()="Hello Cloudron!"]')).then(function () { done(); });
    }

    function checkMedia(done) {
        superagent.get(mediaLink).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.statusCode).to.be(200);
            done();
        });
    }

    function checkPost(done) {
        browser.get('https://' + app.fqdn);
        browser.findElement(by.xpath('//h2/a[text()="Hello Cloudron!"]')).then(function () { done(); });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            token = result.body.token;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                email = result.body.email;
                done();
            });
        });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can login', login);

    it('is an admin dashboard', function (done) {
        browser.wait(until.elementLocated(by.xpath('//div[@class="wp-menu-name" and contains(text(), "Plugins")]')), TIMEOUT).then(function () { done(); });
    });

    it('can edit', function (done) {
        browser.get('https://' + app.fqdn + '/wp-admin/post.php?post=1&action=edit');
        browser.wait(until.elementLocated(by.xpath('//input[@id="title"]')), TIMEOUT);
        browser.findElement(by.xpath('//input[@id="title"]')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        browser.findElement(by.xpath('//input[@id="title"]')).sendKeys('Hello Cloudron!');
        browser.findElement(by.xpath('//input[@id="publish"]')).click();
        browser.wait(until.elementLocated(by.xpath('//*[contains(text(), "Post updated.")]')), TIMEOUT).then(function () { done(); });
    });

    it('can upload media', function (done) {
        browser.get('https://' + app.fqdn + '/wp-admin/media-new.php?browser-uploader');
        browser.wait(until.elementLocated(by.id('async-upload')), TIMEOUT).then(function () {
            browser.findElement(by.xpath('//input[@id="async-upload" and @type="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
            browser.findElement(by.id('html-upload')).click();

            browser.wait(function () {
                return browser.getCurrentUrl().then(function (url) {
                    return url === 'https://' + app.fqdn + '/wp-admin/upload.php';
                });
            }, TIMEOUT).then(function () { done(); });
        });
    });

    var mediaLink;

    it('can see media', function (done) {
        browser.get('https://' + app.fqdn + '/wp-admin/upload.php?item=5'); // there's got to be a better way..
        browser.wait(until.elementLocated(by.xpath('//*[text()="Attachment Details"]')), TIMEOUT);
        browser.findElement(by.xpath('//img[@class="details-image"]')).getAttribute('src').then(function (srcLink) {
            console.log('media is located at ', srcLink);
            mediaLink = srcLink;
            done();
        });
    });

    it('has correct htaccess', checkHtaccess);
    it('can access permalink', checkPermalink);

    it('can restart app', function (done) {
        execSync('cloudron restart');
        done();
    });

    it('can see updated post', checkPost);
    it('can see media', checkMedia);
    it('has correct htaccess', checkHtaccess);
    it('can access permalink', checkPermalink);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can see updated post', checkPost);
    it('can see media', checkMedia);
    it('has correct htaccess', checkHtaccess);
    it('can access permalink', checkPermalink);
    it('can login', login);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron install --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
        mediaLink = mediaLink.replace(LOCATION, LOCATION + '2');
    });

    it('can see updated post', checkPost);
    it('can see media', checkMedia);
    it('has correct htaccess', checkHtaccess);
    it('can access permalink', checkPermalink);
    it('can login', login);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // check if the _first_ login via email succeeds
    it('can login via email', function (done) {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');

        login(function () {
            execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            done();
        });
    });
});

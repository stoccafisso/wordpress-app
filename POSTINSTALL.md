This app integrates with Cloudron SSO. It allows any logged in Cloudron user 
to access the WordPress dashboard. Cloudron administrators are automatically 
made WordPress administrators. Non-administrators get the `editor` role by
default.

You can access the dashboard directly at `/wp-admin/` (trailing slash is
important!).


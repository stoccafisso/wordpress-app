#!/bin/bash

set -eu

readonly WP="/app/code/wp --allow-root"

# Settings to be updated on every run. Regenerating salts means users have to relogin
sed -e "s/##MYSQL_DATABASE/${MYSQL_DATABASE}/" \
    -e "s/##MYSQL_USERNAME/${MYSQL_USERNAME}/" \
    -e "s/##MYSQL_PASSWORD/${MYSQL_PASSWORD}/" \
    -e "s/##MYSQL_HOST/${MYSQL_HOST}:${MYSQL_PORT}/" \
    -e "s,##APP_ORIGIN,${APP_ORIGIN}," \
    -e "s/##AUTH_KEY/$(pwgen -1cns 64)/" \
    -e "s/##SECURE_AUTH_KEY/$(pwgen -1cns 64)/" \
    -e "s/##LOGGED_IN_KEY/$(pwgen -1cns 64)/" \
    -e "s/##NONCE_KEY/$(pwgen -1cns 64)/" \
    -e "s/##AUTH_SALT/$(pwgen -1cns 64)/" \
    -e "s/##SECURE_AUTH_SALT/$(pwgen -1cns 64)/" \
    -e "s/##LOGGED_IN_SALT/$(pwgen -1cns 64)/" \
    -e "s/##NONCE_SALT/$(pwgen -1cns 64)/" \
    /app/code/wp-config.php.template > /run/wordpress/wp-config.php # sed -i seems to destroy symlink

# Used for wp rewrite
touch /app/data/htaccess

if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "Copying wp-content files on first run"
    mkdir -p /app/data/wp-content/mu-plugins
    cp -r /app/code/wp-content-vanilla/* /app/data/wp-content/

    readonly admin_password=$(pwgen -1y 16)
    readonly admin_email=${MAIL_SMTP_USERNAME}@${MAIL_DOMAIN}
    echo "Admin password is ${admin_password} and email is ${admin_email}"

    # --skip-email is part of 0.23.0 https://github.com/wp-cli/wp-cli/pull/2345 and https://github.com/wp-cli/wp-cli/issues/1164
    $WP --url="${APP_ORIGIN}" --skip-email core install \
        --url="${APP_ORIGIN}" \
        --title="My blog" \
        --admin_user=admin \
        --admin_password="${admin_password}" \
        --admin_email="${admin_email}"
    echo "WP is now installed"

    # Set default post structure to what most people want
    # Curiously, installing some plugins prevents .htaccess getting written
    $WP rewrite structure --hard '/%postname%/'

    touch "/app/data/.dbsetup"
else
    # Update wordpress
    echo "Updating wordpress database"
    $WP core update-db
fi

# install and backup the plugins. mu plugins are a "flat" structure
# sadly mu-plugins can still be re-configured, just not uninstallable
# We have to do this on every run to get plugin updates
if [[ ! -f "/run/wordpress/plugins_unpacked" ]]; then
    echo "Unpacking plugins"

    # clear the directory, otherwise unzip/mv have to be forced
    rm -rf /app/data/wp-content/mu-plugins/*

    unzip -d /app/data/wp-content/mu-plugins/ /app/code/disable-wordpress-updates.zip
    mv /app/data/wp-content/mu-plugins/disable-wordpress-updates/* /app/data/wp-content/mu-plugins/
    rm -rf /app/data/wp-content/mu-plugins/disable-wordpress-updates/

    unzip -d /app/data/wp-content/mu-plugins/ /app/code/wp-mail-smtp.zip
    mv /app/data/wp-content/mu-plugins/wp-mail-smtp/* /app/data/wp-content/mu-plugins/
    rm -rf /app/data/wp-content/mu-plugins/wp-mail-smtp/

    unzip -d /app/data/wp-content/mu-plugins/ /app/code/authLdap.zip
    mv /app/data/wp-content/mu-plugins/authLdap-*/* /app/data/wp-content/mu-plugins/
    rm -rf /app/data/wp-content/mu-plugins/authLdap-*/

    touch /run/wordpress/plugins_unpacked
else
    echo "Plugins already unpacked from previous run" # restarts
fi

# configure WP mail smtp plugin (smtp_user, smtp_pass can be set when supported)
echo "Configuring smtp mail"
$WP option update mailer smtp
$WP option update mail_from "${MAIL_FROM}"
$WP option update mail_from_name WordPress
$WP option update smtp_host ${MAIL_SMTP_SERVER}
$WP option update smtp_port ${MAIL_SMTP_PORT}
$WP option update smtp_auth true
$WP option update smtp_user ${MAIL_SMTP_USERNAME}
$WP option update smtp_pass "${MAIL_SMTP_PASSWORD}"

# configure LDAP
# https://github.com/heiglandreas/authLdap/blob/master/authLdap.php#L644
echo "Configuring LDAP"
ldapConfig=$(cat <<EOF
{
    "Enabled"       : true,
    "CachePW"       : false,
    "URI"           : "ldap://${LDAP_SERVER}:${LDAP_PORT}/${LDAP_USERS_BASE_DN}",
    "Filter"        : "(|(mail=%1\$s)(username=%1\$s))",
    "NameAttr"      : "givenName",
    "SecName"       : "sn",
    "UidAttr"       : "username",
    "MailAttr"      : "mail",
    "WebAttr"       : "",
    "Groups"        : { "administrator" : "cn=admins,${LDAP_GROUPS_BASE_DN}" },
    "GroupSeparator": ";",
    "Debug"         : false,
    "GroupAttr"     : "memberof",
    "GroupFilter"   : "(|(mail=%1\$s)(username=%1\$s))",
    "DefaultRole"   : "editor",
    "GroupEnable"   : true,
    "GroupOverUser" : true,
    "Version"       : 1
}
EOF
)
$WP --format=json option update authLDAPOptions "${ldapConfig}"

chown -R www-data:www-data /app/data /run/wordpress

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

